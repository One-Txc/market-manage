

## 🔥 在线资料

- 在线文档：[http://sa-plus.dev33.cn/](http://sa-plus.dev33.cn/)

- 演示地址1：[http://demo-jj.dev33.cn/spdj-admin/index.html](http://demo-jj.dev33.cn/spdj-admin/index.html) （iframe版本）

- 演示地址2：[http://demo-jj.dev33.cn/sps_vue-element-admin/index.html](http://demo-jj.dev33.cn/sps_vue-element-admin/index.html) （Vue单页版本）

- QQ交流群：[310293485 点击加入](https://jq.qq.com/?_wv=1027&k=NNBSOkeA)


## 😘 项目优点 
0. 封装 JavaWeb 常见功能：文件上传、角色授权、Redis控制台、API日志统计、跨域处理 等等。
1. 内置代码生成器，一键生成：普通input、多行文本域、富文本编辑器、日期控件、图片上传、音频上传、视频上传、 多图上传、树形表格、聚合外键、接口文档......
2. 提供两套UI皮肤：基于 iframe 的纯html版、基于 vue-cli 的单页版，适合不同团队技术栈。
3. 提供 API 文档编写工具，可一键生成接口文档，并提供在线测试接口能力。
4. 提供微服务版实现，定时同步更新：[https://gitee.com/click33/sp-cloud](https://gitee.com/click33/sp-cloud)


## ⚡ 功能架构 
- 项目基于 `SpringBoot` 搭建，以 `Freemarker` 作为代码生成器模板 
- 权限验证基于 Sa-Token：[http://sa-token.dev33.cn/](http://sa-token.dev33.cn/)
- 皮肤1基于 Sa-Admin 魔改适配：[https://gitee.com/click33/sa-admin](https://gitee.com/click33/sa-admin)
- 皮肤2基于 vue-element-admin 魔改适配：[https://github.com/PanJiaChen/vue-element-admin](https://github.com/PanJiaChen/vue-element-admin)
- 接口文档基于sa-doc：[http://sa-doc.dev33.cn/](http://sa-doc.dev33.cn/)
- ...... 


## ❤️ 需求提交 
- **我们深知一个优秀的项目需要海纳百川，[点我在线提交需求](http://sa-app.dev33.cn/wall.html?name=sa-plus)**


## 💦 演示预览 
<table>
    <tr>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-1.png"/></td>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-3.png"/></td>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-4.png"/></td>
    </tr>
    <tr>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-5.png"/></td>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-6.png"/></td>
    </tr>
    <tr>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-7.png"/></td>
        <td><img src="https://color-test.oss-cn-qingdao.aliyuncs.com/sa-plus/pre-8.png"/></td>
    </tr>
</table>



