package com.pj.project4sp.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: description
 * @author: tangxiaocheng
 * @create: 2023-03-16 10:45
 */
@Data
@Accessors(chain = true)
public class BaseEntity implements Serializable {

    @TableId(type = IdType.AUTO)
    public Long id;

    /**
     * 创建日期
     */
    public Date createTime;

    /**
     * 更新日期
     */
    public Date updateTime;
}
