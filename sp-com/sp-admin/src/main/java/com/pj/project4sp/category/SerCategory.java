package com.pj.project4sp.category;

import java.io.Serializable;
import java.util.*;

import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_category -- 分类表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
public class SerCategory extends BaseEntity {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_category";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-category";	


	// ---------- 表中字段 ----------
	/**
	 * 分类名称 
	 */
	public String name;	

	/**
	 * 备注 
	 */
	public String remark;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	




}
