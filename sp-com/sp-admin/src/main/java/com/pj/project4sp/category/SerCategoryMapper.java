package com.pj.project4sp.category;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import com.pj.utils.so.*;
import org.springframework.stereotype.Repository;

/**
 * Mapper: ser_category -- 分类表
 * @author tangxc 
 */

@Mapper
@Repository
public interface SerCategoryMapper extends BaseMapper<SerCategory> {


	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<SerCategory> getList(SoMap so);


}
