package com.pj.project4sp.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_category -- 分类表
 * @author tangxc 
 *
 */
@Component
public class SerCategoryUtil {

	
	/** 底层 Mapper 对象 */
	public static SerCategoryMapper serCategoryMapper;
	@Autowired
	private void setSerCategoryMapper(SerCategoryMapper serCategoryMapper) {
		SerCategoryUtil.serCategoryMapper = serCategoryMapper;
	}
	
	
	/** 
	 * 将一个 SerCategory 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerCategory s) {
		AjaxError.throwByIsNull(s.id, "[分类id] 不能为空");		// 验证: 分类id 
		AjaxError.throwByIsNull(s.name, "[分类名称] 不能为空");		// 验证: 分类名称 
		AjaxError.throwByIsNull(s.remark, "[备注] 不能为空");		// 验证: 备注 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerCategory (方便复制代码用) [G] 
	 */ 
	static SerCategory getSerCategory() {
		SerCategory s = new SerCategory();	// 声明对象 
		s.id = 0L;		// 分类id 
		s.name = "";		// 分类名称 
		s.remark = "";		// 备注 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
