package com.pj.project4sp.customer;

import java.io.Serializable;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_customer -- 客户表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
@TableName(SerCustomer.TABLE_NAME)
@EqualsAndHashCode(callSuper = false)
public class SerCustomer  extends BaseEntity {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_customer";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-customer";	


	// ---------- 表中字段 ----------
	/**
	 * 客户id 
	 */
	@TableId(type = IdType.AUTO)
	public Long id;	

	/**
	 * 客户名称 
	 */
	public String name;	

	/**
	 * 联系电话 
	 */
	public String phone;	

	/**
	 * 收货地址 
	 */
	public String address;	

	/**
	 * 用户性别 (1=男, 2=女, 3=未知) 
	 */
	public Integer sex;	

	/**
	 * 年龄 
	 */
	public Integer age;	

	/**
	 * 商品备注 
	 */
	public String remark;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	





	


}
