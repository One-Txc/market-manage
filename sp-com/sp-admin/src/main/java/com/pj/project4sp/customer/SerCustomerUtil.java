package com.pj.project4sp.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_customer -- 客户表
 * @author tangxc 
 *
 */
@Component
public class SerCustomerUtil {

	
	/** 底层 Mapper 对象 */
	public static SerCustomerMapper serCustomerMapper;
	@Autowired
	private void setSerCustomerMapper(SerCustomerMapper serCustomerMapper) {
		SerCustomerUtil.serCustomerMapper = serCustomerMapper;
	}
	
	
	/** 
	 * 将一个 SerCustomer 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerCustomer s) {
		AjaxError.throwByIsNull(s.id, "[客户id] 不能为空");		// 验证: 客户id 
		AjaxError.throwByIsNull(s.name, "[客户名称] 不能为空");		// 验证: 客户名称 
		AjaxError.throwByIsNull(s.phone, "[联系电话] 不能为空");		// 验证: 联系电话 
		AjaxError.throwByIsNull(s.address, "[收货地址] 不能为空");		// 验证: 收货地址 
		AjaxError.throwByIsNull(s.sex, "[用户性别] 不能为空");		// 验证: 用户性别 (1=男, 2=女, 3=未知) 
		AjaxError.throwByIsNull(s.age, "[年龄] 不能为空");		// 验证: 年龄 
		AjaxError.throwByIsNull(s.remark, "[商品备注] 不能为空");		// 验证: 商品备注 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerCustomer (方便复制代码用) [G] 
	 */ 
	static SerCustomer getSerCustomer() {
		SerCustomer s = new SerCustomer();	// 声明对象 
		s.id = 0L;		// 客户id 
		s.name = "";		// 客户名称 
		s.phone = "";		// 联系电话 
		s.address = "";		// 收货地址 
		s.sex = 0;		// 用户性别 (1=男, 2=女, 3=未知) 
		s.age = 0;		// 年龄 
		s.remark = "";		// 商品备注 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
