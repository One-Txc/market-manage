package com.pj.project4sp.enter;

import java.io.Serializable;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_enter -- 入库表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
@TableName(SerEnter.TABLE_NAME)
@EqualsAndHashCode(callSuper = false)
public class SerEnter  extends BaseEntity {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_enter";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-enter";	


	// ---------- 表中字段 ----------
	/**
	 * 入库id 
	 */
	@TableId(type = IdType.AUTO)
	public Long id;	

	/**
	 * 入库商品 
	 */
	public Long goodsId;	

	/**
	 * 入库数量 
	 */
	public Integer enterCount;	

	/**
	 * 入库价格 
	 */
	public Integer money;	

	/**
	 * 入库日期 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date enterTime;	

	/**
	 * 操作人 
	 */
	public Long createUid;	

	/**
	 * 备注 
	 */
	public String remark;

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	



	// ---------- 额外字段 ----------
	/**
	 * 入库商品 
	 */
	@TableField(exist = false)
	public String serGoodsName;

	/**
	 * 商品分类
	 */
	@TableField(exist = false)
	public String serCategoryName;

	/**
	 * 操作人 
	 */
	@TableField(exist = false)
	public String spAdminName;

	/**
	 * 供应商
	 */
	@TableField(exist = false)
	public String serSupplierName;

	/**
	 * 供应商
	 */
	@TableField(exist = false)
	public Integer totalMoney;

	public Integer getTotalMoney() {
		return enterCount*money;
	}
}
