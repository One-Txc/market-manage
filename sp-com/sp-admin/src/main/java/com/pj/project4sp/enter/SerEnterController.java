package com.pj.project4sp.enter;

import java.util.List;

import com.pj.project4sp.admin.SpAdmin;
import com.pj.project4sp.admin.SpAdminMapper;
import com.pj.project4sp.category.SerCategory;
import com.pj.project4sp.category.SerCategoryMapper;
import com.pj.project4sp.goods.SerGoods;
import com.pj.project4sp.goods.SerGoodsMapper;
import com.pj.project4sp.supplier.SerSupplier;
import com.pj.project4sp.supplier.SerSupplierMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.pj.utils.sg.*;
import com.pj.utils.so.*;
import com.pj.project4sp.SP;

import com.pj.current.satoken.StpUserUtil;
import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: ser_enter -- 入库表
 * @author tangxc 
 */
@RestController
@RequestMapping("/SerEnter/")
public class SerEnterController {

	/** 底层 Mapper 对象 */
	@Autowired
	SerEnterMapper serEnterMapper;
	@Autowired
	SerGoodsMapper serGoodsMapper;
	@Autowired
	SerCategoryMapper serCategoryMapper;
	@Autowired
	SerSupplierMapper serSupplierMapper;
	@Autowired
	SpAdminMapper spAdminMapper;

	/** 增 */  
	@RequestMapping("add")
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(SerEnter s){
		serEnterMapper.insert(s);
		// 更新库存
		serGoodsMapper.updateStock(s.getGoodsId(), s.getEnterCount());
		return AjaxJson.getSuccessData(s);
	}

	/** 删 */  
	@RequestMapping("delete")
	public AjaxJson delete(Long id){
		int line = serEnterMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}

	/** 撤销 */
	@RequestMapping("rollback")
	public AjaxJson rollback(Long id){
		// 更新库存
		SerEnter s = serEnterMapper.selectById(id);
		int c = serGoodsMapper.updateStock(s.getGoodsId(), -s.getEnterCount());
		if (c <= 0) {
			return AjaxJson.getError("库存不足，无法撤销");
		}

		int line = serEnterMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}
	
	/** 删 - 根据id列表 */  
	@RequestMapping("deleteByIds")
	public AjaxJson deleteByIds(){
		List<Long> ids = SoMap.getRequestSoMap().getListByComma("ids", long.class); 
		int line = SP.publicMapper.deleteByIds(SerEnter.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}
	
	/** 改 */  
	@RequestMapping("update")
	public AjaxJson update(SerEnter s){
		int line = serEnterMapper.updateById(s);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */  
	@RequestMapping("getById")
	public AjaxJson getById(Long id){
		SerEnter s = serEnterMapper.selectById(id);
		SerGoods serGoods = serGoodsMapper.selectById(s.getGoodsId());
		if(serGoods != null){
			s.setSerGoodsName(serGoods.getName());
			SerCategory serCategory = serCategoryMapper.selectById(serGoods.getCategoryId());
			if(serCategory != null){
				s.setSerCategoryName(serCategory.getName());
			}
			SerSupplier serSupplier = serSupplierMapper.selectById(serGoods.getSupplierId());
			if(serSupplier != null){
				s.setSerSupplierName(serSupplier.getName());
			}
		}
		SpAdmin admin = spAdminMapper.getById(s.getCreateUid());
		if(admin != null){
			s.setSpAdminName(admin.getName());
		}

		return AjaxJson.getSuccessData(s);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */  
	@RequestMapping("getList")
	public AjaxJson getList() { 
		SoMap so = SoMap.getRequestSoMap();
		List<SerEnter> list = serEnterMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}
	
	
	
	// ------------------------- 前端接口 -------------------------
	
	
	/** 改 - 不传不改 [G] */
	@RequestMapping("updateByNotNull")
	public AjaxJson updateByNotNull(Long id){
		AjaxError.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者 
		long userId = SP.publicMapper.getColumnByIdToLong(SerEnter.TABLE_NAME, "user_id", id);
		AjaxError.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		SoMap so = SoMap.getRequestSoMap();
		so.clearNotIn("id", "goodsId", "enterCount", "money", "enterTime", "createUid", "remark", "createTime", "updateTime").clearNull().humpToLineCase();	
		int line = SP.publicMapper.updateBySoMapById(SerEnter.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}
	
	
	
	
	
	

}
