package com.pj.project4sp.enter;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pj.utils.so.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * Mapper: ser_enter -- 入库表
 * @author tangxc 
 */

@Mapper
@Repository
public interface SerEnterMapper  extends BaseMapper<SerEnter> {

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<SerEnter> getList(SoMap so);

}
