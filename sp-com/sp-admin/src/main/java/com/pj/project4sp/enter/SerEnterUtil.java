package com.pj.project4sp.enter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_enter -- 入库表
 * @author tangxc 
 *
 */
@Component
public class SerEnterUtil {

	
	/** 底层 Mapper 对象 */
	public static SerEnterMapper serEnterMapper;
	@Autowired
	private void setSerEnterMapper(SerEnterMapper serEnterMapper) {
		SerEnterUtil.serEnterMapper = serEnterMapper;
	}
	
	
	/** 
	 * 将一个 SerEnter 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerEnter s) {
		AjaxError.throwByIsNull(s.id, "[入库id] 不能为空");		// 验证: 入库id 
		AjaxError.throwByIsNull(s.goodsId, "[入库商品] 不能为空");		// 验证: 入库商品 
		AjaxError.throwByIsNull(s.enterCount, "[入库数量] 不能为空");		// 验证: 入库数量 
		AjaxError.throwByIsNull(s.money, "[入库价格] 不能为空");		// 验证: 入库价格 
		AjaxError.throwByIsNull(s.enterTime, "[入库日期] 不能为空");		// 验证: 入库日期 
		AjaxError.throwByIsNull(s.createUid, "[操作人] 不能为空");		// 验证: 操作人 
		AjaxError.throwByIsNull(s.remark, "[备注] 不能为空");		// 验证: 备注 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerEnter (方便复制代码用) [G] 
	 */ 
	static SerEnter getSerEnter() {
		SerEnter s = new SerEnter();	// 声明对象 
		s.id = 0L;		// 入库id 
		s.goodsId = 0L;		// 入库商品 
		s.enterCount = 0;		// 入库数量 
		s.money = 0;		// 入库价格 
		s.enterTime = new Date();		// 入库日期 
		s.createUid = 0L;		// 操作人 
		s.remark = "";		// 备注 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
