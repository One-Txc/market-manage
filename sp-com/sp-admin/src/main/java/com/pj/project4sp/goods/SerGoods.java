package com.pj.project4sp.goods;

import java.io.Serializable;
import java.util.*;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_goods -- 商品表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
@TableName(SerGoods.TABLE_NAME)
@EqualsAndHashCode(callSuper = false)
public class SerGoods  extends BaseEntity {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_goods";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-goods";	


	// ---------- 表中字段 ----------
	/**
	 * 商品id 
	 */
	@TableId(type = IdType.AUTO)
	public Long id;	

	/**
	 * 商品编号 
	 */
	public String goodsNo;	

	/**
	 * 商品名称 
	 */
	public String name;	

	/**
	 * 商品规格 
	 */
	public String spec;	

	/**
	 * 商品图片 
	 */
	public String avatar;	

	/**
	 * 分类id 
	 */
	public Long categoryId;	

	/**
	 * 供应商id 
	 */
	public Long supplierId;	

	/**
	 * 商品备注 
	 */
	public String remark;	

	/**
	 * 商品价格 
	 */
	public Integer money;	

	/**
	 * 剩余库存 
	 */
	public Integer stockCount;	

	/**
	 * 商品状态 (1=上架,2=下架) 
	 */
	public Integer status;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	



	// ---------- 额外字段 ----------
	/**
	 * 所属分类 
	 */
	@TableField(exist = false)
	public String serCategoryName;	

	/**
	 * 供应商 
	 */
	@TableField(exist = false)
	public String serSupplierName;	



	


}
