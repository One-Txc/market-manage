package com.pj.project4sp.goods;

import java.util.List;

import com.pj.project4sp.category.SerCategory;
import com.pj.project4sp.category.SerCategoryMapper;
import com.pj.project4sp.supplier.SerSupplier;
import com.pj.project4sp.supplier.SerSupplierMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.pj.utils.sg.*;
import com.pj.utils.so.*;
import com.pj.project4sp.SP;

import com.pj.current.satoken.StpUserUtil;
import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: ser_goods -- 商品表
 * @author tangxc 
 */
@RestController
@RequestMapping("/SerGoods/")
public class SerGoodsController {

	/** 底层 Mapper 对象 */
	@Autowired
	SerGoodsMapper serGoodsMapper;
	@Autowired
	SerCategoryMapper serCategoryMapper;
	@Autowired
	SerSupplierMapper serSupplierMapper;


	/** 增 */  
	@RequestMapping("add")
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(SerGoods s){
		serGoodsMapper.insert(s);
		return AjaxJson.getSuccessData(s);
	}

	/** 删 */  
	@RequestMapping("delete")
	public AjaxJson delete(Long id){
		int line = serGoodsMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}
	
	/** 删 - 根据id列表 */  
	@RequestMapping("deleteByIds")
	public AjaxJson deleteByIds(){
		List<Long> ids = SoMap.getRequestSoMap().getListByComma("ids", long.class); 
		int line = SP.publicMapper.deleteByIds(SerGoods.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}
	
	/** 改 */  
	@RequestMapping("update")
	public AjaxJson update(SerGoods s){
		int line = serGoodsMapper.updateById(s);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */  
	@RequestMapping("getById")
	public AjaxJson getById(Long id){
		SerGoods s = serGoodsMapper.selectById(id);

		if (s.getCategoryId() != null){
			SerCategory category = serCategoryMapper.selectById(s.getCategoryId());
			if (category != null) {
				s.setSerCategoryName(category.getName());
			}
		}
		if (s.getSupplierId() != null){
			SerSupplier serSupplier = serSupplierMapper.selectById(s.getSupplierId());
			if (serSupplier != null) {
				s.setSerSupplierName(serSupplier.getName());
			}
		}
		return AjaxJson.getSuccessData(s);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */  
	@RequestMapping("getList")
	public AjaxJson getList() { 
		SoMap so = SoMap.getRequestSoMap();
		List<SerGoods> list = serGoodsMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}
	
	
	
	// ------------------------- 前端接口 -------------------------
	
	
	/** 改 - 不传不改 [G] */
	@RequestMapping("updateByNotNull")
	public AjaxJson updateByNotNull(Long id){
		AjaxError.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者 
		long userId = SP.publicMapper.getColumnByIdToLong(SerGoods.TABLE_NAME, "user_id", id);
		AjaxError.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		SoMap so = SoMap.getRequestSoMap();
		so.clearNotIn("id", "goodsNo", "name", "spec", "avatar", "categoryId", "supplierId", "remark", "money", "stockCount", "status", "createTime", "updateTime").clearNull().humpToLineCase();	
		int line = SP.publicMapper.updateBySoMapById(SerGoods.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}
	
	
	
	
	
	

}
