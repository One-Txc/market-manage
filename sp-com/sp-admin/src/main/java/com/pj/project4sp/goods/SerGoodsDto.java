package com.pj.project4sp.goods;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * Model: ser_goods -- 商品表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
public class SerGoodsDto extends SerGoods {




}
