package com.pj.project4sp.goods;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pj.utils.so.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Mapper: ser_goods -- 商品表
 * @author tangxc 
 */

@Mapper
@Repository
public interface SerGoodsMapper  extends BaseMapper<SerGoods> {

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<SerGoods> getList(SoMap so);

	/**
	 * 库存跟新
	 * @param goodsId
	 * @param changeCount
	 * @return
	 */
	int updateStock( @Param("goodsId")Long goodsId, @Param("changeCount") Integer changeCount);

	List<SerGoodsDto> categoryReport();

	List<SerGoodsDto> supplierReport();
}
