package com.pj.project4sp.goods;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_goods -- 商品表
 * @author tangxc 
 *
 */
@Component
public class SerGoodsUtil {

	
	/** 底层 Mapper 对象 */
	public static SerGoodsMapper serGoodsMapper;
	@Autowired
	private void setSerGoodsMapper(SerGoodsMapper serGoodsMapper) {
		SerGoodsUtil.serGoodsMapper = serGoodsMapper;
	}
	
	
	/** 
	 * 将一个 SerGoods 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerGoods s) {
		AjaxError.throwByIsNull(s.id, "[商品id] 不能为空");		// 验证: 商品id 
		AjaxError.throwByIsNull(s.goodsNo, "[商品编号] 不能为空");		// 验证: 商品编号 
		AjaxError.throwByIsNull(s.name, "[商品名称] 不能为空");		// 验证: 商品名称 
		AjaxError.throwByIsNull(s.spec, "[商品规格] 不能为空");		// 验证: 商品规格 
		AjaxError.throwByIsNull(s.avatar, "[商品图片] 不能为空");		// 验证: 商品图片 
		AjaxError.throwByIsNull(s.categoryId, "[分类id] 不能为空");		// 验证: 分类id 
		AjaxError.throwByIsNull(s.supplierId, "[供应商id] 不能为空");		// 验证: 供应商id 
		AjaxError.throwByIsNull(s.remark, "[商品备注] 不能为空");		// 验证: 商品备注 
		AjaxError.throwByIsNull(s.money, "[商品价格] 不能为空");		// 验证: 商品价格 
		AjaxError.throwByIsNull(s.stockCount, "[剩余库存] 不能为空");		// 验证: 剩余库存 
		AjaxError.throwByIsNull(s.status, "[商品状态] 不能为空");		// 验证: 商品状态 (1=上架,2=下架) 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerGoods (方便复制代码用) [G] 
	 */ 
	static SerGoods getSerGoods() {
		SerGoods s = new SerGoods();	// 声明对象 
		s.id = 0L;		// 商品id 
		s.goodsNo = "";		// 商品编号 
		s.name = "";		// 商品名称 
		s.spec = "";		// 商品规格 
		s.avatar = "";		// 商品图片 
		s.categoryId = 0L;		// 分类id 
		s.supplierId = 0L;		// 供应商id 
		s.remark = "";		// 商品备注 
		s.money = 0;		// 商品价格 
		s.stockCount = 0;		// 剩余库存 
		s.status = 0;		// 商品状态 (1=上架,2=下架) 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
