package com.pj.project4sp.out;

import java.util.List;

import com.pj.project4sp.admin.SpAdmin;
import com.pj.project4sp.admin.SpAdminMapper;
import com.pj.project4sp.category.SerCategory;
import com.pj.project4sp.category.SerCategoryMapper;
import com.pj.project4sp.customer.SerCustomer;
import com.pj.project4sp.customer.SerCustomerMapper;
import com.pj.project4sp.enter.SerEnter;
import com.pj.project4sp.goods.SerGoods;
import com.pj.project4sp.goods.SerGoodsMapper;
import com.pj.project4sp.supplier.SerSupplier;
import com.pj.project4sp.supplier.SerSupplierMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.pj.utils.sg.*;
import com.pj.utils.so.*;
import com.pj.project4sp.SP;

import com.pj.current.satoken.StpUserUtil;
import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: ser_out -- 出库表
 * @author tangxc 
 */
@RestController
@RequestMapping("/SerOut/")
public class SerOutController {

	/** 底层 Mapper 对象 */
	@Autowired
	SerOutMapper serOutMapper;
	@Autowired
	SerGoodsMapper serGoodsMapper;
	@Autowired
	SerCategoryMapper serCategoryMapper;
	@Autowired
	SerSupplierMapper serSupplierMapper;
	@Autowired
	SpAdminMapper spAdminMapper;
	@Autowired
	SerCustomerMapper serCustomerMapper;

	/** 增 */  
	@RequestMapping("add")
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(SerOut s){
		// 更新库存
		int c = serGoodsMapper.updateStock(s.getGoodsId(), -s.getOutCount());
		if (c <= 0) {
			return AjaxJson.getError("库存不足，无法出库");
		}
		serOutMapper.insert(s);
		return AjaxJson.getSuccessData(s);
	}

	/** 删 */  
	@RequestMapping("delete")
	public AjaxJson delete(Long id){
		int line = serOutMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}
	
	/** 删 - 根据id列表 */  
	@RequestMapping("deleteByIds")
	public AjaxJson deleteByIds(){
		List<Long> ids = SoMap.getRequestSoMap().getListByComma("ids", long.class); 
		int line = SP.publicMapper.deleteByIds(SerOut.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}

	/** 撤销 */
	@RequestMapping("rollback")
	public AjaxJson rollback(Long id){
		// 更新库存
		SerOut s = serOutMapper.selectById(id);
		int c = serGoodsMapper.updateStock(s.getGoodsId(), s.getOutCount());

		int line = serOutMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}
	
	/** 改 */  
	@RequestMapping("update")
	public AjaxJson update(SerOut s){
		int line = serOutMapper.updateById(s);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */  
	@RequestMapping("getById")
	public AjaxJson getById(Long id){
		SerOut s = serOutMapper.selectById(id);
		SerGoods serGoods = serGoodsMapper.selectById(s.getGoodsId());
		if(serGoods != null){
			s.setSerGoodsName(serGoods.getName());
			SerCategory serCategory = serCategoryMapper.selectById(serGoods.getCategoryId());
			if(serCategory != null){
				s.setSerCategoryName(serCategory.getName());
			}
			SerSupplier serSupplier = serSupplierMapper.selectById(serGoods.getSupplierId());
			if(serSupplier != null){
				s.setSerSupplierName(serSupplier.getName());
			}
		}
		SpAdmin admin = spAdminMapper.getById(s.getCreateUid());
		if(admin != null){
			s.setSpAdminName(admin.getName());
		}
		SerCustomer customer = serCustomerMapper.selectById(s.getCustomerId());
		if(customer != null){
			s.setSerCustomerName(customer.getName());
		}
		return AjaxJson.getSuccessData(s);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */  
	@RequestMapping("getList")
	public AjaxJson getList() { 
		SoMap so = SoMap.getRequestSoMap();
		List<SerOut> list = serOutMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}
	
	
	
	// ------------------------- 前端接口 -------------------------
	
	
	/** 改 - 不传不改 [G] */
	@RequestMapping("updateByNotNull")
	public AjaxJson updateByNotNull(Long id){
		AjaxError.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者 
		long userId = SP.publicMapper.getColumnByIdToLong(SerOut.TABLE_NAME, "user_id", id);
		AjaxError.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		SoMap so = SoMap.getRequestSoMap();
		so.clearNotIn("id", "customerId", "goodsId", "outCount", "money", "outTime", "createUid", "remark", "createTime", "updateTime").clearNull().humpToLineCase();	
		int line = SP.publicMapper.updateBySoMapById(SerOut.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}
	
	
	
	
	
	

}
