package com.pj.project4sp.out;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pj.utils.so.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * Mapper: ser_out -- 出库表
 * @author tangxc 
 */

@Mapper
@Repository
public interface SerOutMapper  extends BaseMapper<SerOut> {

	/**
	 * 查集合 - 根据条件（参数为空时代表忽略指定条件）
	 * @param so 参数集合 
	 * @return 数据列表 
	 */
	List<SerOut> getList(SoMap so);

	List<SerOut> goodsOutReport(SoMap so);
}
