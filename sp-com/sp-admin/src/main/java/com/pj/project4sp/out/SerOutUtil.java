package com.pj.project4sp.out;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_out -- 出库表
 * @author tangxc 
 *
 */
@Component
public class SerOutUtil {

	
	/** 底层 Mapper 对象 */
	public static SerOutMapper serOutMapper;
	@Autowired
	private void setSerOutMapper(SerOutMapper serOutMapper) {
		SerOutUtil.serOutMapper = serOutMapper;
	}
	
	
	/** 
	 * 将一个 SerOut 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerOut s) {
		AjaxError.throwByIsNull(s.id, "[出库id] 不能为空");		// 验证: 出库id 
		AjaxError.throwByIsNull(s.customerId, "[客户] 不能为空");		// 验证: 客户 
		AjaxError.throwByIsNull(s.goodsId, "[出库商品] 不能为空");		// 验证: 出库商品 
		AjaxError.throwByIsNull(s.outCount, "[出库数量] 不能为空");		// 验证: 出库数量 
		AjaxError.throwByIsNull(s.money, "[出库价格] 不能为空");		// 验证: 出库价格 
		AjaxError.throwByIsNull(s.outTime, "[出库日期] 不能为空");		// 验证: 出库日期 
		AjaxError.throwByIsNull(s.createUid, "[操作人] 不能为空");		// 验证: 操作人 
		AjaxError.throwByIsNull(s.remark, "[备注] 不能为空");		// 验证: 备注 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerOut (方便复制代码用) [G] 
	 */ 
	static SerOut getSerOut() {
		SerOut s = new SerOut();	// 声明对象 
		s.id = 0L;		// 出库id 
		s.customerId = 0L;		// 客户 
		s.goodsId = 0L;		// 出库商品 
		s.outCount = 0;		// 出库数量 
		s.money = 0;		// 出库价格 
		s.outTime = new Date();		// 出库日期 
		s.createUid = 0L;		// 操作人 
		s.remark = "";		// 备注 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
