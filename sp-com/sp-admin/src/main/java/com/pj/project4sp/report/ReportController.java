package com.pj.project4sp.report;

import com.pj.project4sp.goods.SerGoods;
import com.pj.project4sp.goods.SerGoodsDto;
import com.pj.project4sp.goods.SerGoodsMapper;
import com.pj.project4sp.out.SerOut;
import com.pj.project4sp.out.SerOutMapper;
import com.pj.utils.sg.AjaxJson;
import com.pj.utils.so.SoMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: description
 * @author: tangxiaocheng
 * @create: 2023-03-18 19:18
 */
@RestController
@RequestMapping("/report/")
public class ReportController {

    @Autowired
    SerGoodsMapper serGoodsMapper;
    @Autowired
    SerOutMapper serOutMapper;

    @RequestMapping("goodsStockReport")
    public AjaxJson getList() {
        SoMap so = new SoMap();
        so.put("sortType", 8);
        List<SerGoods> list = serGoodsMapper.getList(so);
        return AjaxJson.getPageData(-1L, list);
    }

    @RequestMapping("categoryReport")
    public AjaxJson categoryReport() {
        List<SerGoodsDto> list = serGoodsMapper.categoryReport();
        return AjaxJson.getSuccessData(list);
    }

    @RequestMapping("supplierReport")
    public AjaxJson supplierReport() {
        List<SerGoodsDto> list = serGoodsMapper.supplierReport();
        return AjaxJson.getSuccessData(list);
    }


    @RequestMapping("goodsOutReport")
    public AjaxJson goodsOutReport() {
        SoMap so = SoMap.getRequestSoMap();
        List<SerOut> list = serOutMapper.goodsOutReport(so);
        return AjaxJson.getPageData(so.getDataCount(), list);

    }

}
