package com.pj.project4sp.supplier;

import java.io.Serializable;
import java.util.*;

import com.pj.project4sp.base.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Model: ser_supplier -- 供应商表
 * @author tangxc 
 */
@Data
@Accessors(chain = true)
public class SerSupplier extends BaseEntity {

	// ---------- 模块常量 ----------
	/**
	 * 序列化版本id 
	 */
	private static final long serialVersionUID = 1L;	
	/**
	 * 此模块对应的表名 
	 */
	public static final String TABLE_NAME = "ser_supplier";	
	/**
	 * 此模块对应的权限码 
	 */
	public static final String PERMISSION_CODE = "ser-supplier";	


	// ---------- 表中字段 ----------

	/**
	 * 名称 
	 */
	public String name;	

	/**
	 * 地址 
	 */
	public String address;	

	/**
	 * 联系人 
	 */
	public String contactsName;	

	/**
	 * 联系电话 
	 */
	public String contactsPhone;	

	/**
	 * 备注 
	 */
	public String remark;	

	/**
	 * 创建日期 
	 */
	public Date createTime;	

	/**
	 * 更新日期 
	 */
	public Date updateTime;	





	


}
