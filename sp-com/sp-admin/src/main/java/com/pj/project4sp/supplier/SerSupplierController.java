package com.pj.project4sp.supplier;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.pj.project4sp.goods.SerGoods;
import com.pj.project4sp.goods.SerGoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.pj.utils.sg.*;
import com.pj.utils.so.*;
import com.pj.project4sp.SP;

import com.pj.current.satoken.StpUserUtil;
import cn.dev33.satoken.annotation.SaCheckPermission;


/**
 * Controller: ser_supplier -- 供应商表
 * @author tangxc 
 */
@RestController
@RequestMapping("/SerSupplier/")
public class SerSupplierController {

	@Autowired
	SerGoodsMapper serGoodsMapper;

	/** 底层 Mapper 对象 */
	@Autowired
	SerSupplierMapper serSupplierMapper;

	/** 增 */  
	@RequestMapping("add")
	@Transactional(rollbackFor = Exception.class)
	public AjaxJson add(SerSupplier s){
		serSupplierMapper.insert(s);
		return AjaxJson.getSuccessData(s);
	}

	/** 删 */  
	@RequestMapping("delete")
	public AjaxJson delete(Long id){
		LambdaQueryWrapper<SerGoods> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SerGoods::getSupplierId, id);
		int count = serGoodsMapper.selectCount(queryWrapper);
		if (count>0) {
			return AjaxJson.getError("该供应商下有商品，不能删除");
		}
		int line = serSupplierMapper.deleteById(id);
		return AjaxJson.getByLine(line);
	}
	
	/** 删 - 根据id列表 */  
	@RequestMapping("deleteByIds")
	public AjaxJson deleteByIds(){
		List<Long> ids = SoMap.getRequestSoMap().getListByComma("ids", long.class); 
		int line = SP.publicMapper.deleteByIds(SerSupplier.TABLE_NAME, ids);
		return AjaxJson.getByLine(line);
	}
	
	/** 改 */  
	@RequestMapping("update")
	public AjaxJson update(SerSupplier s){
		int line = serSupplierMapper.updateById(s);
		return AjaxJson.getByLine(line);
	}

	/** 查 - 根据id */  
	@RequestMapping("getById")
	public AjaxJson getById(Long id){
		SerSupplier s = serSupplierMapper.selectById(id);
		return AjaxJson.getSuccessData(s);
	}

	/** 查集合 - 根据条件（参数为空时代表忽略指定条件） */  
	@RequestMapping("getList")
	public AjaxJson getList() { 
		SoMap so = SoMap.getRequestSoMap();
		List<SerSupplier> list = serSupplierMapper.getList(so.startPage());
		return AjaxJson.getPageData(so.getDataCount(), list);
	}

	
	// ------------------------- 前端接口 -------------------------
	
	
	/** 改 - 不传不改 [G] */
	@RequestMapping("updateByNotNull")
	public AjaxJson updateByNotNull(Long id){
		AjaxError.throwBy(true, "如需正常调用此接口，请删除此行代码");
		// 鉴别身份，是否为数据创建者 
		long userId = SP.publicMapper.getColumnByIdToLong(SerSupplier.TABLE_NAME, "user_id", id);
		AjaxError.throwBy(userId != StpUserUtil.getLoginIdAsLong(), "此数据您无权限修改");
		// 开始修改 (请只保留需要修改的字段)
		SoMap so = SoMap.getRequestSoMap();
		so.clearNotIn("id", "name", "address", "contactsName", "contactsPhone", "remark", "createTime", "updateTime").clearNull().humpToLineCase();	
		int line = SP.publicMapper.updateBySoMapById(SerSupplier.TABLE_NAME, so, id);
		return AjaxJson.getByLine(line);
	}
	
	
	
	
	
	

}
