package com.pj.project4sp.supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pj.utils.sg.*;
import java.util.*;

/**
 * 工具类：ser_supplier -- 供应商表
 * @author tangxc 
 *
 */
@Component
public class SerSupplierUtil {

	
	/** 底层 Mapper 对象 */
	public static SerSupplierMapper serSupplierMapper;
	@Autowired
	private void setSerSupplierMapper(SerSupplierMapper serSupplierMapper) {
		SerSupplierUtil.serSupplierMapper = serSupplierMapper;
	}
	
	
	/** 
	 * 将一个 SerSupplier 对象进行进行数据完整性校验 (方便add/update等接口数据校验) [G] 
	 */
	static void check(SerSupplier s) {
		AjaxError.throwByIsNull(s.id, "[id] 不能为空");		// 验证: id 
		AjaxError.throwByIsNull(s.name, "[名称] 不能为空");		// 验证: 名称 
		AjaxError.throwByIsNull(s.address, "[地址] 不能为空");		// 验证: 地址 
		AjaxError.throwByIsNull(s.contactsName, "[联系人] 不能为空");		// 验证: 联系人 
		AjaxError.throwByIsNull(s.contactsPhone, "[联系电话] 不能为空");		// 验证: 联系电话 
		AjaxError.throwByIsNull(s.remark, "[备注] 不能为空");		// 验证: 备注 
		AjaxError.throwByIsNull(s.createTime, "[创建日期] 不能为空");		// 验证: 创建日期 
		AjaxError.throwByIsNull(s.updateTime, "[更新日期] 不能为空");		// 验证: 更新日期 
	}

	/** 
	 * 获取一个SerSupplier (方便复制代码用) [G] 
	 */ 
	static SerSupplier getSerSupplier() {
		SerSupplier s = new SerSupplier();	// 声明对象 
		s.id = 0L;		// id 
		s.name = "";		// 名称 
		s.address = "";		// 地址 
		s.contactsName = "";		// 联系人 
		s.contactsPhone = "";		// 联系电话 
		s.remark = "";		// 备注 
		s.createTime = new Date();		// 创建日期 
		s.updateTime = new Date();		// 更新日期 
		return s;
	}
	
	
	
	
	
}
