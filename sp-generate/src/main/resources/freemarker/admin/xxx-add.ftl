<template>
	<div class="vue-box sbot sbot-fluid">
		<!-- ------- 内容部分 ------- -->
			<div class="c-panel">
				<el-form v-if="m">
	<#list t.t12List as c>
	<#if c.istx('no-add')>
	<#elseif c.foType == 'logic-delete'>
	<#elseif c.getFlag() == 'tree-parent-id'>
					<sa-item type="text" name="${c.columnComment3}" v-model="m.${c.fieldName}" v-if="sa.p('${t.getTreeFkey()}', 'nof') == 'nof'" br></sa-item>
	<#elseif c.foType == 'text'>
					<sa-item type="text" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'num'>
					<sa-item type="num" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'textarea'>
					<sa-item type="textarea" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'richtext'>
					<sa-item type="richtext" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'enum'>
					<sa-item type="enum" name="${c.columnComment3}" v-model="m.${c.fieldName}" :jv="${c.getJvJson()}" jtype="${c.gtx('a-type')}" br></sa-item>
	<#elseif c.foType == 'img'>
					<sa-item type="img" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'audio'>
					<sa-item type="audio" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'video'>
					<sa-item type="video" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'file'>
					<sa-item type="file" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'img-list'>
					<sa-item type="img-list" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.isFoType('audio-list', 'video-list', 'file-list', 'img-video-list')>
					<sa-item type="${c.foType}" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'date'>
					<sa-item type="datetime" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.isFoType('date-create', 'date-update')>
					<!-- ${c.foType}字段： m.${c.fieldName} - ${c.columnComment3} -->
	<#elseif c.foType == 'time'>
					<sa-item type="time" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	<#elseif c.foType == 'fk-s'>
			<#if c.istx('drop')>
					<sa-item name="${c.columnComment3}" br>
						<el-select v-model="m.${c.fkSCurrDc.fieldName}">
							<el-option label="请选择" value="" disabled></el-option>
							<el-option v-for="item in ${c.fieldName}List" :label="item.${c.tx.catc}" :value="item.${c.tx.jc}" :key="item.${c.tx.jc}"></el-option>
						</el-select>
					</sa-item>
			</#if>
	<#elseif c.foType == 'no'>
					<!-- no字段： m.${c.fieldName} - ${c.columnComment3} -->
	<#else>
					<sa-item type="text" name="${c.columnComment3}" v-model="m.${c.fieldName}" br></sa-item>
	</#if>
	</#list>
				</el-form>
			</div>
	</div>
</template>
<script>
export default
{
	name: '${t.kebabName}-add',
	props: { param: Object },
	data () {
		return {
			id: (this.param && this.param.id) || 0, 	// 超级对象
			m: this.createModel(),
			<#list t.getT2DropList() as c>
			${c.fieldName}List: [],		// ${c.columnComment} 集合
			</#list>
		}
	},
	methods: {
		// 创建一个 默认Model
		createModel: function() {
			return {
		<#list t.t1List as c>
			<#if c.getFlag() == 'tree-parent-id'>
				${c.fieldName}: sa.p('${c.fieldName}', '${t.getFt('tree', 'tree-lazy').top}'),		// ${c.columnComment}
			<#elseif c.isFoType('no', 'logic-delete', 'date-create', 'date-update')>
				// ${c.fieldName}: '',		// ${c.columnComment}
			<#elseif c.isFoType('img-list', 'audio-list', 'video-list', 'file-list', 'img-video-list')>
				${c.fieldName}: '',		// ${c.columnComment}
			<#else>
				${c.fieldName}: '',		// ${c.columnComment}
			</#if>
		</#list>
			}
		},
		// 提交数据
		ok: function(){
			// 表单校验
			let m = this.m;
	<#list t.t1List as c>
		<#if c.istx('no-add') || c.isFoType('no', 'logic-delete', 'date-create', 'date-update')>
			// sa.checkNull(m.${c.fieldName}, '请输入 [${c.columnComment3}]');
		<#else>
			sa.checkNull(m.${c.fieldName}, '请输入 [${c.columnComment3}]');
		</#if>
	</#list>

			// 开始增加或修改
		<#list t.getT1ListByNotAdd() as c>
			// this.m.${c.fieldName} = undefined;		// 不提交属性：${c.columnComment3}
		</#list>
			if(this.id <= 0) {	// 添加
				sa.ajax('/${t.mkNameBig}/add', m, function(res){
					sa.alert('增加成功', this.clean);
				}.bind(this));
			} else {	// 修改
				sa.ajax('/${t.mkNameBig}/update', m, function(res){
					sa.alert('修改成功', this.clean);
				}.bind(this));
			}
		},
		// 添加/修改 完成后的动作
		clean: function() {
			if(this.id == 0) {
				this.m = this.createModel();
			} else {
				sa.closeModel();
				sa.currView().f5();
<#if t.hasFt('tree-lazy')>
				sa.currView().f5();
</#if>
			}
		}
	},
	mounted: function(){
		// 初始化数据
		if(this.id <= 0) {
			this.m = this.createModel();
		} else {
			sa.ajax('/${t.mkNameBig}/getById?id=' + this.id, function(res) {
				this.m = res.data;
				if(res.data == null) {
					sa.alert('未能查找到 id=' + this.id + " 详细数据");
				}
			}.bind(this))
		}
<#if t.getT2DropList()?size != 0>

		// ------------- 加载所需外键列表 -------------
	<#list t.getT2DropList() as c>
		// 加载 ${c.columnComment}
		sa.ajax('/${c.getJtMkName()}/getList?pageSize=1000', function(res) {
			this.${c.fieldName}List = res.data; // 数据集合
		}.bind(this), {msg: null});
	</#list>
</#if>
	}
}

</script>