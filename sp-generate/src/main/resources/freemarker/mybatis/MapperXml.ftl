<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${t.packagePath}.${t.mkNameBig}Mapper">


	<!-- 查集合 - 根据条件（参数为空时代表忽略指定条件） [G] -->
	<select id="getList" resultType="${t.packagePath}.${t.mkNameBig}">
		select ${t.getT1ListCatStringOrStar()}<#if t.getT2List()?size != 0 || t.getT3List()?size != 0>,</#if>
		<#list t.getT2List() as c>
			${c.getT2Sql()}<#if c_index != t.getT2List()?size-1 || t.getT3List()?size != 0>, </#if>
		</#list>
		<#list t.getT3List() as c>
			${c.getT3Sql()}<#if c_index != t.getT3List()?size-1>, </#if>
		</#list>
		from `${t.tableName}`
		<where>
${t.getT1List_ByMapperGetListWhere()}<#rt>
		</where>
		order by
		<choose>
${t.getT1List_ByMapperGetListSort()}<#rt>
		</choose>
	</select>
	
	
	
	
	
	
	
	
	

</mapper>
