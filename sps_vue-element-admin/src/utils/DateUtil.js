/**
 * Created by jackphang on 2017/9/19 8:29
 */

// import { $t } from '@/main'

/**
 * @fileName:date-util.ts
 * @author:jackphang
 * @dateTime:2017/9/19 8:29
 * @desc:日期工具类
 */
export default class DateUtil {
  static isNullOrUndefined (v) {
    if (v === null || v === undefined) {
      return true
    }
    return false
  }

  /**
   * 毫秒数转换为指定格式的字符串
   * @param ts 日期对象毫秒数, 如果缺省，
   * @param format 日期格式,默认格式定义如下 yyyy-MM-dd(默认)
   *
   * YYYY/yyyy/YY/yy 表示年份
   * MM/M 月份
   * W/w 星期
   * dd/DD/d/D 日期
   * hh/HH/h/H 时间
   * mm/m 分钟
   * ss/SS/s/S 秒
   * @return string 指定格式的时间字符串
   */
  static formatDateByTs (ts, format) {
    if (DateUtil.isNullOrUndefined(ts)) {
      return ''
    }
    let str = (format || 'yyyy-MM-dd')
    const date = new Date(ts)
    const Week = ['日', '一', '二', '三', '四', '五', '六']
    str = str.replace(/yyyy|YYYY/, date.getFullYear().toString())
    str = str.replace(/yy|YY/, (date.getFullYear() % 100) > 9 ? (date.getFullYear() % 100).toString() : '0' + (date.getFullYear() % 100))
    str = str.replace(/MM/, date.getMonth() + 1 > 9 ? (date.getMonth() + 1).toString() : '0' + (date.getMonth() + 1))
    str = str.replace(/M/g, date.getMonth().toString())
    str = str.replace(/w|W/g, Week[date.getDay()])

    str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate())
    str = str.replace(/d|D/g, date.getDate().toString())

    str = str.replace(/hh|HH/, date.getHours() > 9 ? date.getHours().toString() : '0' + date.getHours())
    str = str.replace(/h|H/g, date.getHours().toString())
    str = str.replace(/mm/, date.getMinutes() > 9 ? date.getMinutes().toString() : '0' + date.getMinutes())
    str = str.replace(/m/g, date.getMinutes().toString())

    str = str.replace(/ss|SS/, date.getSeconds() > 9 ? date.getSeconds().toString() : '0' + date.getSeconds())
    str = str.replace(/s|S/g, date.getSeconds().toString())

    return str
  }

  /**
   * 格式化日期 yyyy-MM-dd
   */
  static formatDate (value) {
    return DateUtil.formatDateByTs(value, 'yyyy-MM-dd')
  }

  /**
   * 格式化日期是yyyy-MM-dd HH:mm
   */
  static formatDateMin (value) {
    return DateUtil.formatDateByTs(value, 'yyyy-MM-dd HH:mm')
  }

  /**
   * 格式化日期是yyyy-MM-dd HH:mm:ss
   */
  static formatDateTime (value) {
    return DateUtil.formatDateByTs(value, 'yyyy-MM-dd HH:mm:ss')
  }

  /**
   * 格式化日期是MM-dd HH:mm
   */
  static formatDateMonthDayHourMinute (value) {
    return DateUtil.formatDateByTs(value, 'MM-dd HH:mm')
  }

  /* 获取当前日期 yyyy-MM-dd */
  static getNowDate () {
    var date = new Date()
    return DateUtil.formatDate(date.getTime())
  }

  /* 获取当前日期 yyyy-MM-dd HH:mm */
  static getNowDateMin () {
    var date = new Date()
    return DateUtil.formatDateMin(date.getTime())
  }

  /* 获取当前日期 yyyy-MM-dd HH:mm:ss */
  static getNowDateTime () {
    var date = new Date()
    return DateUtil.formatDateTime(date.getTime())
  }

  static formatDateToString (date, format) {
    let str = (format || 'yyyy-MM-dd')
    const Week = ['日', '一', '二', '三', '四', '五', '六']
    str = str.replace(/yyyy|YYYY/, date.getFullYear().toString())
    str = str.replace(/yy|YY/, (date.getFullYear() % 100) > 9 ? (date.getFullYear() % 100).toString() : '0' + (date.getFullYear() % 100))
    str = str.replace(/MM/, date.getMonth() + 1 > 9 ? (date.getMonth() + 1).toString() : '0' + (date.getMonth() + 1))
    str = str.replace(/M/g, date.getMonth().toString())
    str = str.replace(/w|W/g, Week[date.getDay()])

    str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate())
    str = str.replace(/d|D/g, date.getDate().toString())

    str = str.replace(/hh|HH/, date.getHours() > 9 ? date.getHours().toString() : '0' + date.getHours())
    str = str.replace(/h|H/g, date.getHours().toString())
    str = str.replace(/mm/, date.getMinutes() > 9 ? date.getMinutes().toString() : '0' + date.getMinutes())
    str = str.replace(/m/g, date.getMinutes().toString())

    str = str.replace(/ss|SS/, date.getSeconds() > 9 ? date.getSeconds().toString() : '0' + date.getSeconds())
    str = str.replace(/s|S/g, date.getSeconds().toString())

    return str
  }

  /**
   *js中更改日期
   * @param: date 日期对象
   * @param: y-年， m-月， d-日， h-小时， n-分钟，s-秒
   */
  static add (date, part, value) {
    value *= 1
    if (isNaN(value)) {
      value = 0
    }
    switch (part) {
      case 'y':
        date.setFullYear(date.getFullYear() + value)
        return date
      case 'm':
        date.setMonth(date.getMonth() + value)
        return date
      case 'd':
        date.setDate(date.getDate() + value)
        return date
      case 'h':
        date.setHours(date.getHours() + value)
        return date
      case 'n':
        date.setMinutes(date.getMinutes() + value)
        return date
      case 's':
        date.setSeconds(date.getSeconds() + value)
        return date
      default:
        return date
    }
  }

  static getWeekDaysChn (value) {
    const weekDays = {
      0: '星期日',
      1: '星期一',
      2: '星期二',
      3: '星期三',
      4: '星期四',
      5: '星期五',
      6: '星期六'
    }
    const weekDaysChn = []
    value.forEach((value) => {
      weekDaysChn.push(weekDays[value])
    })
    return weekDaysChn
  }

  /**
   * 获取本周的第一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheFirstOfThisWeek () {
    var now = new Date() // 当前日期
    var nowDayOfWeek = now.getDay() // 今天本周的第几天
    var nowDay = now.getDate() // 当前日
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年
    var weekStartDate = new Date(nowYear, nowMonth, nowDay - nowDayOfWeek)
    return DateUtil.formatDate(weekStartDate.getTime())
  }

  /**
   * 获取本周的最后一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheEndOfThisWeek () {
    var now = new Date() // 当前日期
    var nowDayOfWeek = now.getDay() // 今天本周的第几天
    var nowDay = now.getDate() // 当前日
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年
    var weekEndDate = new Date(nowYear, nowMonth, nowDay + (6 - nowDayOfWeek))
    return DateUtil.formatDate(weekEndDate.getTime())
  }

  /**
   * 获取本月的第一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheFirstOfThisMonth () {
    var now = new Date() // 当前日期
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年

    var monthStartDate = new Date(nowYear, nowMonth, 1)
    return DateUtil.formatDate(monthStartDate.getTime())
  }

  /**
   * 获取上一个月的最后一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheEndOfPreviousMonth () {
    var now = new Date() // 当前日期
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年
    var monthStartDate = new Date(nowYear, nowMonth, 0)
    return DateUtil.formatDate(monthStartDate.getTime())
  }

  /**
   * 获取上一个月的最后一天或者当天(如果当天是月末则获取当天) yyyy-MM-dd
   * @returns {string}
   */
  static getTheEndOfPreviousMonthOrNow () {
    var now = new Date() // 当前日期
    var nowMonth = now.getMonth() // 当前月
    if (now.getDate() === this.getMonthDays(now, nowMonth)) {
      return DateUtil.getTheEndOfThisMonth()
    } else {
      return DateUtil.getTheEndOfPreviousMonth()
    }
  }

  /**
   * 获取上一个月的第一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheFirstOfPreviousMonth () {
    var now = new Date() // 当前日期
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年

    var monthStartDate = new Date(nowYear, nowMonth - 1, 1)
    return DateUtil.formatDate(monthStartDate.getTime())
  }

  /**
   * 获取某个月的最后一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheEndOfMonth (date) {
    var now = date // 当前日期
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年
    var monthEndDate = new Date(nowYear, nowMonth, this.getMonthDays(now, nowMonth))
    return DateUtil.formatDate(monthEndDate.getTime())
  }

  /**
   * 获取本月的最后一天 yyyy-MM-dd
   * @returns {string}
   */
  static getTheEndOfThisMonth () {
    var now = new Date() // 当前日期
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年

    var monthEndDate = new Date(nowYear, nowMonth, this.getMonthDays(now, nowMonth))
    return DateUtil.formatDate(monthEndDate.getTime())
  }

  /**
   * 获得某月的天数
   * @param month
   * @returns {number}
   */
  static getMonthDays (date, month) {
    var now = date // 当前日期
    var nowYear = now.getFullYear() // 当前年
    var monthStartDate = new Date(nowYear, month, 1)
    var monthEndDate = new Date(nowYear, month + 1, 1)
    var days = (monthEndDate.getTime() - monthStartDate.getTime()) / (1000 * 60 * 60 * 24)
    return days
  }

  /**
   * 获取当前时间的前/后几天 yyyy-MM-dd
   * @param {number} increment
   * @returns {string}
   */
  static getOneDate (increment) {
    var now = new Date() // 当前日期
    var nowDay = now.getDate() // 当前日
    var nowMonth = now.getMonth() // 当前月
    var nowYear = now.getFullYear() // 当前年
    var date = new Date(nowYear, nowMonth, nowDay + increment)
    return DateUtil.formatDate(date.getTime())
  }

  /**
   * 字符串转date,
   * 字符串位置要求：1-4代表年,6-7代表月,9-10代表日,12-13代表时,15-16代表分，18-19代表秒,最短长度是10
   * @param dateStr
   * @returns {string}
   */
  static stringToDate (dateStr) {
    if (dateStr === null) {
      return
    }
    dateStr = dateStr.trim()
    const year = Number(dateStr.substring(0, 4))
    const month = Number(dateStr.substring(5, 7))
    const day = Number(dateStr.substring(8, 10))
    let hour = 0
    let minute = 0
    let second = 0
    if (dateStr.length >= 13) {
      hour = Number(dateStr.substring(11, 13))
    }
    if (dateStr.length >= 16) {
      minute = Number(dateStr.substring(14, 16))
    }
    if (dateStr.length >= 19) {
      second = Number(dateStr.substring(17, 19))
    }
    var date = new Date(year, month - 1, day, hour, minute, second)
    return date
  }
}
