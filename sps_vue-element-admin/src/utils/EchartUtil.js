

export default class EchartUtil {

  static getBaseOption (xAxisData,seriesData,name) {
    let option = {
      tooltip: {//鼠标悬浮提示数据
        trigger:'axis'
      },
      toolbox: {
        show: true,
        feature: {
          magicType: {
            type: ["bar", "line"]
          },
        }
      },
      legend: {
        data: [name]
      },
      xAxis: {
        data: xAxisData
      },
      yAxis: {},
      series: [
        {
          name: name,
          type: 'bar',
          itemStyle : { normal: {label : {show: true}}},   //重点是这句加上了后可以显示数字。不然不显示
          data: seriesData
        }
      ]
    };
    return option
  }

  static getBasePieOption (seriesData) {
    let option = {
      tooltip: {//鼠标悬浮提示数据
        trigger:'axis'
      },
      series: [
        {
          type: 'pie',
          data: seriesData,
          radius: '50%',
          //百分比
          label: {
            normal: {
              show: true,
              formatter: '{b}\n{c}\n{d}%' //自定义显示格式(b:name, c:value, d:百分比)
            }
          },
        }
      ]
    };
    return option
  }

}
